/**
 * Created by naor on 24/1/17.
 */
import { Template } from 'meteor/templating';

import './stop-watch.html';

Template.stopWatch.onRendered(function () {
    const el = this.$('.watch-container');

    let rafRequest;

    el.html('Loading...')
    // Reactively changing the odometer's value
    Template.instance().autorun(() => {
        const minute = Template.currentData().minute;
        const hour = Template.currentData().hour;
        const SPEED = Template.currentData().speed;

        // console.log('stop watch', minute, hour);

        if (rafRequest) {
            window.cancelAnimationFrame(rafRequest);
            rafRequest = null;
        }
        if (minute === -1 || hour === -1) return;

        const hourText = hour > 9 ? hour.toString() : '0' + hour;
        const minuteText = minute > 9 ? minute.toString() : '0' + minute;

        const changeTS = Date.now();

        renderText(hourText, minuteText, '00');

        rafRequest = requestAnimationFrame(function refresh() {
            const currMs = Date.now() - changeTS;

            if (currMs < 1000 / SPEED) {
                let secondsText = (Math.round((currMs / (1000 / SPEED) * 60)) - 1).toString();
                if (secondsText.length === 1) {
                    secondsText = '0' + secondsText;
                }

                renderText(hourText, minuteText, secondsText);

                rafRequest = requestAnimationFrame(refresh);
            }
        });
    });

    function renderText(h, m, s) {
        // console.log('writing text', h, m, s);

        el.html(h + ':' + m /*+ ':' + s*/ + ' GMT');
    }
});