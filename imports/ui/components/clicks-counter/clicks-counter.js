/**
 * Created by naor on 1/2/17.
 */

import { Template } from 'meteor/templating';

import './clicks-counter.html';

Template.clicksCounter.onRendered(function () {
    const el = this.$('.counter-text');

    let rafRequest, lastValue = 0;

    // Reactively changing the odometer's value
    Template.instance().autorun(() => {
        const currentValue = Template.currentData().total;
        const SPEED = Template.currentData().speed;

        // console.log('stop watch', minute, hour);

        if (rafRequest) {
            window.cancelAnimationFrame(rafRequest);
            rafRequest = null;
        }

        // const hourText = hour > 9 ? hour.toString() : '0' + hour;
        // const minuteText = minute > 9 ? minute.toString() : '0' + minute;

        if (currentValue < lastValue) {
            lastValue = currentValue;
            return;
        }

        const changeTS = Date.now();
        const currDiff = currentValue - lastValue;
        // console.log('currentValue', currentValue);


        // renderText(hourText, minuteText, '00');

        rafRequest = requestAnimationFrame(function refresh() {
            const currMs = Date.now() - changeTS;

            if (currMs < 1000 / SPEED) {
                lastValue = currentValue + Math.round(currDiff * (currMs / (1000 / SPEED)));
                // lastValue += Math.round(currDiff * (currMs / (1000 / SPEED)));
                // const temporalVal = lastValue
                const temporalVal = lastValue
                    .toString()
                    .split('')
                    .reverse()
                    .reduce((arr, char, index) => {
                        if (index > 0 && index % 3 === 0) {
                            arr.push(',');
                        }

                        arr.push(char);

                        return arr;
                    }, [])
                    .reverse()
                    .join('');

                renderText(temporalVal);

                rafRequest = requestAnimationFrame(refresh);
            }
        });
    });

    function renderText(numStr) {
        // console.log('writing text', h, m, s);

        el.html(numStr);
    }
});