/**
 * Created by naor on 1/2/17.
 */

import d3 from 'd3';

export default function darkSide() {

    const lngOffset = 10;
    return (planet) => {
        let totalMinutes = 0;
        planet.plugins.darkSide = {
            setTime(m, h) {
                totalMinutes = m + h * 60;
            }
        };
        planet.onDraw(() => {
            let currAngle = (totalMinutes/1440) * 360;
            if (currAngle > 180) {
                currAngle = currAngle - 360;
            }

            const currOffset = lngOffset - currAngle ;
            planet.withSavedContext((ctx) => {
                function drawCircle(angle) {
                    const circle = d3.geo.circle().origin([currOffset, 0])
                        .angle(angle)();
                    ctx.beginPath();
                    planet.path.context(ctx)(circle);
                    ctx.fill();
                }
                ctx.fillStyle = 'rgba(0, 0, 0, 0.2)';
                drawCircle(82);
                drawCircle(84);
                drawCircle(86);
                drawCircle(88);
            });
        })
    };
}