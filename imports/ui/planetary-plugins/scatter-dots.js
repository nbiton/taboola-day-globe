/**
 * Created by naor on 1/2/17.
 */
import d3 from 'd3';

export default function scatterDots(options) {
    options = options || {};
    const TTL = options.ttl || 1000;
    let descriptors = {};
    let idCounter = 0;
    const wideRad = 2.5;
    const closeRad = 0.75;
    let liveDots = [];
    const colors = [
        '#ffe930',
        '#f3b85d',
        '#6a6afc',
        '#64b8ef',
        '#c9e665',
        '#60c7ad'
    ].map(hex => `rgb(${parseInt(hex.slice(1, 3), 16)}, ${parseInt(hex.slice(3, 5), 16)}, ${parseInt(hex.slice(5,7), 16)})`);

    return (planet) => {
        planet.plugins.scatterDots = {
            draw(lat, lng, dots) {
                const id = idCounter++;
                descriptors[id] = {lat, lng, dots, drawn: 0, start: Date.now(), id: idCounter, live: [], remainder: 0};
            },
            reset() {
                descriptors = {};
                idCounter = 0;
                liveDots = [];
            }
        };

        planet.onDraw(() => {
            const currTs = Date.now();
            const currRotation = planet.projection.rotate();

            // Fixing lng rotation to the +180 to -180 range, to match the coordinate space
            if (Math.abs(currRotation[0]) >= 180) currRotation[0] -= 360 * (Math.abs(currRotation[0]) / currRotation[0]);

            let lngChecker;
            if (currRotation[0] + 90 > 180) {
                const minLng = currRotation[0] - 90,
                    maxLng = -270 + currRotation[0];
                lngChecker = function (lng) {
                    lng *= -1;
                    return lng > minLng || lng < maxLng
                }
            } else if (currRotation[0] - 90 < -180) {
                const minLng = 270 + currRotation[0],
                    maxLng = currRotation[0] + 90;
                lngChecker = function (lng) {
                    lng *= -1;
                    return lng > minLng || lng < maxLng;
                }
            } else {
                lngChecker = function (lng) {
                    return Math.abs(lng + currRotation[0]) < 90
                };
            }

            planet.withSavedContext((context) => {
                liveDots = liveDots.reduce((all, dot) => {
                    dot.frames++;
                    const alpha = (10 - Math.floor(10 * dot.frames / 10 )) / 10;
                    const color = `rgba(${dot.color.r}, ${dot.color.g}, ${dot.color.b}, ${alpha})`;
                    drawDot(dot.loc[0], dot.loc[1], color, dot.size);
                    if (dot.frames < 10) {
                        all.push(dot);
                    }

                    return all;
                }, []);

                Object.values(descriptors).forEach((desc) => {

                    if (currTs - desc.start > TTL) {
                        delete descriptors[desc.id];
                        return;
                    }
                    // Assuming 60fps as ideal
                    let dotsPerFrame = desc.dots / (60 / (1000 / TTL));

                    const addedDots = Math.floor(desc.remainder);
                    dotsPerFrame += addedDots;
                    desc.remainder = desc.remainder - addedDots + dotsPerFrame - Math.floor(dotsPerFrame);

                    for (let i = 0; i < dotsPerFrame; i++) {
                        // const scatterRad = Math.random() > 0.75 ? wideRad : closeRad;
                        const scatterRad = Math.pow(Math.random(), 2) * (wideRad - closeRad) + closeRad;
                        const randLat = -scatterRad + Math.random() * (scatterRad + scatterRad + 1);

                        // x must respect x² + y² < r²
                        const xMax = Math.pow(Math.pow(scatterRad, 2) - Math.pow(randLat, 2), 0.5);
                        const randLng = Math.random() * 2 * xMax - xMax;

                        const lng = desc.lng + randLng;
                        const lat = desc.lat + randLat;
                        const randColor = colors[Math.floor(Math.random() * colors.length)];

                        const size = Math.floor(Math.random() * 3 + 1);
                        drawDot(lng, lat, randColor, size);

                        liveDots.push({loc: [lng,lat], size, frames: 0, color: d3.rgb(randColor)});
                    }
                });

                function drawDot(lng, lat, color, size) {
                    // const lngClipped = Math.abs(lng + currRotation[0]) > 90;
                    const lngIncluded = lngChecker(lng);
                    const latClipped = Math.abs(lat + currRotation[1]) > 90;

                    if (lngIncluded && !latClipped) {
                        const projected = planet.projection([lng, lat]);

                        context.fillStyle = color;
                        context.fillRect(projected[0] - size / 2, projected[1] - size / 2, size, size);
                    }
                }
            });
        });
    };
}