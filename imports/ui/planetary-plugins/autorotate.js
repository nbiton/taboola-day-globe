/**
 * Created by naor on 31/1/17.
 */

// This plugin will automatically rotate the globe around its vertical
// axis a configured number of degrees every second.
export default function autorotate(rotationSpeed) {
    // Planetary.js plugins are functions that take a `planet` instance
    // as an argument...
    let degPerSec = rotationSpeed;
    return function(planet) {
        let lastTick = null;
        let paused = false;
        planet.plugins.autorotate = {
            pause:  function() { paused = true;  },
            resume: function() { paused = false; },
            isPaused: function() { return paused; },
            setSpeed: function(newVal) { degPerSec = newVal; }
        };
        // ...and configure hooks into certain pieces of its lifecycle.
        planet.onDraw(function() {
            if (paused || !lastTick) {
                lastTick = new Date();
            } else {
                const now = new Date();
                const delta = now - lastTick;
                // This plugin uses the built-in projection (provided by D3)
                // to rotate the globe each time we draw it.
                const rotation = planet.projection.rotate();
                rotation[0] += degPerSec * delta / 1000;
                if (rotation[0] >= 180) rotation[0] -= 360;
                planet.projection.rotate(rotation);
                lastTick = now;
            }
        });
    };
};