/**
 * Created by naor on 31/1/17.
 */

// Plugin to automatically scale the planet's projection based
// on the window size when the planet is initialized
export default function autoscale(options) {
    options = options || {};
    return function(planet) {
        planet.onInit(function() {
            let width  = window.innerWidth + (options.extraWidth || 0);
            let height = window.innerHeight + (options.extraHeight || 0);
            planet.projection.scale(Math.min(width, height) / 2 - 75);
        });
    };
};
