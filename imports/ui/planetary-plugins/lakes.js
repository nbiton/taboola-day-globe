/**
 * Created by naor on 1/2/17.
 */
import topojson from 'topojson';

// This plugin takes lake data from the special
// TopoJSON we're loading and draws them on the map.
export default function lakes(options) {
    options = options || {};
    let lakes = null;

    return function(planet) {
        planet.onInit(function() {
            // We can access the data loaded from the TopoJSON plugin
            // on its namespace on `planet.plugins`. We're loading a custom
            // TopoJSON file with an object called "ne_110m_lakes".
            const world = planet.plugins.topojson.world;
            lakes = topojson.feature(world, world.objects.ne_110m_lakes);
        });

        planet.onDraw(function() {
            planet.withSavedContext(function(context) {
                context.beginPath();
                planet.path.context(context)(lakes);
                context.fillStyle = options.fill || 'black';
                context.fill();
            });
        });
    };
}