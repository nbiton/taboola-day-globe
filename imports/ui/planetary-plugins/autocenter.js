/**
 * Created by naor on 31/1/17.
 */
import d3 from 'd3';

export default function autocenter(options) {
    options = options || {};
    let needsCentering = false;
    let globe = null;

    const resize = function() {
        let width  = window.innerWidth + (options.extraWidth || 0);
        let height = window.innerHeight + (options.extraHeight || 0);
        globe.canvas.width = width;
        globe.canvas.height = height;
        globe.projection.translate([width / 2, height / 2]);
    };

    return function(planet) {
        globe = planet;
        planet.onInit(function() {
            needsCentering = true;
            d3.select(window).on('resize', function() {
                needsCentering = true;
            });
        });

        planet.onDraw(function() {
            if (needsCentering) { resize(); needsCentering = false; }
        });
    };
}