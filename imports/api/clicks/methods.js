/**
 * Created by naor on 23/1/17.
 */
import clicks from './clicks';

Meteor.methods({
    'clicks.getHourData' (minute, hour) {
        const cursor = clicks.find({
            $or: [
                {
                    'details.Hour': hour,
                    'details.Minute': {
                        $gte: minute
                    }
                },
                {
                    'details.Hour': hour + 1
                },
                {
                    'details.Hour': hour + 2
                },
                {
                    'details.Hour': hour + 3,
                    'details.Minute': {
                        $lt: minute
                    }
                }
            ],
            // value: {
            //     $gte: 10
            // }
        }, {
            sort: {
                'details.Hour': 1,
                'details.Minute': 1
            }
        });

        return cursor.fetch().reduce((all, click) => {
            click.details.lng = parseFloat(click.details.lng.toFixed(2));
            click.details.lat = parseFloat(click.details.lat.toFixed(2));
            Object.assign(click, click.details);
            delete click.details;
            delete click._id;

            all.push(['Hour', 'Minute', 'lng', 'lat', 'value'].reduce((valArr, key) => {
                valArr.push(click[key]);
                return valArr;
            }, []));
            return all;
        }, []);
    }
});