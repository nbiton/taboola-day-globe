import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import d3 from 'd3';
import planetaryjs from 'planetary.js';
import autorotate from '../imports/ui/planetary-plugins/autorotate';
import autocenter from '../imports/ui/planetary-plugins/autocenter';
import autoscale from '../imports/ui/planetary-plugins/autoscale';
import lakes from '../imports/ui/planetary-plugins/lakes';
import scatterDots from '../imports/ui/planetary-plugins/scatter-dots';
import darkSide from '../imports/ui/planetary-plugins/dark-side';

import '../imports/ui/components/stop-watch/stop-watch';
import '../imports/ui/components/clicks-counter/clicks-counter';
import './main.html';

const SPEED = 5;

Template.globe.onCreated(function () {
    this.currentHour = new ReactiveVar(-1);
    this.currentMinute = new ReactiveVar(-1);
    this.clicksTotal = new ReactiveVar(0);
});

Template.globe.onRendered(function () {
    const globe = planetaryjs.planet();

    const clicksArr = [];
    let clickInd = 0;

    globe.loadPlugin(autorotate(-120));
    globe.loadPlugin(autocenter({extraHeight: 0}));
    globe.loadPlugin(autoscale({extraHeight: -120}));

    globe.loadPlugin(planetaryjs.plugins.topojson({
        file:   '/world-110m-withlakes.json'
    }));

    globe.loadPlugin(planetaryjs.plugins.oceans({
        fill: '#0e60a0'
    }));

    globe.loadPlugin(planetaryjs.plugins.land({
        fill: '#073c65'
    }));

    globe.loadPlugin(lakes({
        fill: '#0e60a0'
    }));

    globe.loadPlugin(darkSide());

    globe.loadPlugin(planetaryjs.plugins.borders({
        stroke: '#062238'
    }));

    // The `pings` plugin draws animated pings on the globe.
    // globe.loadPlugin(planetaryjs.plugins.pings());
    globe.loadPlugin(scatterDots({ttl: 1000 / SPEED}));
    // The `zoom` and `drag` plugins enable
    // manipulating the globe with the mouse.
    globe.loadPlugin(planetaryjs.plugins.zoom({
        scaleExtent: [350, 1000]
    }));

    let dragStartTime;
    let lastDragPosition;
    let slowDownInterval;
    globe.loadPlugin(planetaryjs.plugins.drag({
        // Dragging the globe should pause the
        // automatic rotation until we release the mouse.
        onDragStart: function() {

            const currRotation = this.projection.rotate();
            if (Math.abs(currRotation[0]) >= 180) currRotation[0] -= 360 * (Math.abs(currRotation[0]) / currRotation[0]);

            if (slowDownInterval) {
                clearInterval(slowDownInterval);
                slowDownInterval = null;
            }
            this.plugins.autorotate.pause();
        },
        onDrag: function () {
            // console.log('drag evt', arguments, this, d3.event);
            lastDragPosition = this.projection.rotate()[0];
            if (Math.abs(lastDragPosition) >= 180) lastDragPosition -= 360 * (Math.abs(lastDragPosition) / lastDragPosition);

            dragStartTime = Date.now();
        },
        // afterDrag: function () {
        //     console.log(' after rotation ', this.projection.rotate()[0])
        // },
        onDragEnd: function() {
            const totalDragTime = Date.now() - dragStartTime;
            let currPosition = this.projection.rotate()[0];
            if (Math.abs(currPosition) >= 180) currPosition -= 360 * (Math.abs(currPosition) / currPosition);
            let movementTotal;

            if (currPosition / lastDragPosition < 0 && Math.abs(currPosition) + Math.abs(lastDragPosition) > 200) {
                movementTotal = 360 - Math.abs(currPosition) - Math.abs(lastDragPosition);
                movementTotal = lastDragPosition < 0 ? -movementTotal : movementTotal;
            } else {
                movementTotal = currPosition - lastDragPosition;
            }
            let speed = movementTotal * (1000 / totalDragTime) * 0.3;

            if (Math.abs(speed) > 10){
                this.plugins.autorotate.setSpeed(speed);
                slowDownInterval = setInterval(() => {
                    if (Math.abs(speed) - 5 > 10) {
                        speed *= 0.7;
                        this.plugins.autorotate.setSpeed(speed);
                    } else {
                        this.plugins.autorotate.setSpeed(10 * Math.abs(speed) / speed);
                        clearInterval(slowDownInterval);
                        slowDownInterval = null;
                    }
                }, 200);
                this.plugins.autorotate.resume();
            }
        }
    }));

    window.addEventListener('keyup', function (evt) {
        if (evt.keyCode === 32) {
            if (globe.plugins.autorotate.isPaused()) {
                globe.plugins.autorotate.resume();
            } else {
                globe.plugins.autorotate.pause();
            }
        }

    });

    // Set up the globe's initial scale, offset, and rotation.
    globe.projection.scale(220).translate([250, 250]).rotate([0, -10, 0]);

    // const colorsMapper = d3.scale.pow()
    //     .exponent(3)
    //     .domain([50, 100, 200, 350, 550])
    //     .range(['white', 'yellow', 'orange', 'red', 'purple']);

    // Also create a scale for mapping magnitudes to ping angle sizes
    // const angleMapper = d3.scale.pow()
    //     .exponent(1)
    //     .domain([10, 800])
    //     .range([0.5, 10]);

    const self = this;

    let currentTime = [0, 0];
    let reloaded = false;

    const reqStart = Date.now();
    console.log('request for 0,0');
    // const clicksRx = Template.instance().clicksTotal;

    Meteor.call('clicks.getHourData', 0, 0, (err, res) => {

        console.log('request for 0,0 finished in ', Date.now() - reqStart);

        globe.plugins.autorotate.setSpeed(10);
        res.forEach((item) => {
            clicksArr.push(parseClickValueArr(item));
        });

        // const clicksRx = Template.instance().clicksTotal;
        setInterval(() => {
            function hasListEnded() {
                return clickInd >= clicksArr.length;
            }

            if (hasListEnded()) {
                clickInd = 0;
                currentTime = [0, 0];
                self.clicksTotal.set(0);
                globe.plugins.scatterDots.reset();
                reloaded = true;
            }
            const currMinute = currentTime[1];
            const currHour = currentTime[0];

            self.currentHour.set(currentTime[0]);
            self.currentMinute.set(currentTime[1]);

            globe.plugins.darkSide.setTime(currMinute, currHour);
            // let currPos = 0;
            // const startRend = Date.now();
            let renderCount = 0;

            let click;
            let iterationClicksTotal = 0;
            while (! hasListEnded() && clicksArr[clickInd].Hour <= currHour && clicksArr[clickInd].Minute <= currMinute){
                click = clicksArr[clickInd];
                globe.plugins.scatterDots.draw(click.lat, click.lng, click.value);

                iterationClicksTotal += click.value;

                renderCount++;
                clickInd++;
            }

            const currClickCount = self.clicksTotal.get();
            self.clicksTotal.set(currClickCount + iterationClicksTotal);

            if (currentTime[1] === 1 && currentTime[0] % 3 === 0 && currentTime[0] !== 23 && ! reloaded) {
                // subHandle = Meteor.subscribe('click-time-frame', currentTime[0], currentTime[1]);

                const iterReqStart = Date.now();
                const reqHour = currentTime[0] + 3;
                console.log('request for 0,', currentTime[0] + 3);

                Meteor.call('clicks.getHourData', 0, currentTime[0] + 3, (err, res) => {
                    console.log('request for 0,', reqHour, ' finished in ', Date.now() - iterReqStart);

                    res.forEach((item) => {
                        // clicksLocal.insert(item);
                        clicksArr.push(parseClickValueArr(item));
                    });
                });
            }

            if (currentTime[1] < 59) {
                currentTime[1]++;
            } else {
                currentTime[1] = 0;
                currentTime[0]++;
            }
        }, 1000 / SPEED);
    });

    const canvas = document.getElementById('globe');

    // Special code to handle high-density displays (e.g. retina, some phones)
    // In the future, Planetary.js will handle this by itself (or via a plugin).
    if (window.devicePixelRatio == 2) {
        canvas.width = 800;
        canvas.height = 800;
        context = canvas.getContext('2d');
        context.scale(2, 2);
    }
    // Draw that globe!
    globe.draw(canvas);
});

function parseClickValueArr(valArr) {
    return ['Hour', 'Minute', 'lng', 'lat', 'value'].reduce((obj, key) => {
        obj[key] = valArr.shift();
        return obj;
    }, {});
}


Template.globe.helpers({
    currentMinute (){
        return Template.instance().currentMinute.get();
    },
    currentHour (){
        return Template.instance().currentHour.get();
    },
    visSpeed () {
        return SPEED;
    },

    clicksTotal() {
        return Template.instance().clicksTotal.get();
    }
});


