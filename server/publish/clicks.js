/**
 * Created by naor on 12/1/17.
 */
import clicks from '/imports/api/clicks/clicks';
Meteor.publish('click-time-frame', (hour, minute) => {
    // console.log('hour', hour);
    // console.log('minute', minute);

    const cursor = clicks.find({
        $or: [
            {
                'details.Hour': hour,
                'details.Minute': {
                    $gte: minute
                }
            },
            {
                'details.Hour': hour + 1,
                'details.Minute': {
                    $lte: minute
                }
            }
        ],
        value: {
            $gte: 10
        }
    });

    // console.log('total', cursor.fetch().length);
    return cursor;
});
